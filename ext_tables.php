<?php
// TYPO3 Security Check
defined('TYPO3_MODE') or die();

$_EXTKEY = $GLOBALS['_EXTKEY'] = 'ns_faq';

//Add Modules
if (TYPO3_MODE === 'BE') {
    $isVersion9Up = \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger(TYPO3_version) >= 9000000;

    if (version_compare(TYPO3_branch, '8.0', '>=')) {

        // Add module 'nitsan' after 'Web'
        if (!isset($GLOBALS['TBE_MODULES']['nitsan'])) {
            $temp_TBE_MODULES = [];
            foreach ($GLOBALS['TBE_MODULES'] as $key => $val) {
                if ($key == 'web') {
                    $temp_TBE_MODULES[$key] = $val;
                    $temp_TBE_MODULES['nitsan'] = '';
                } else {
                    $temp_TBE_MODULES[$key] = $val;
                }
            }
            $GLOBALS['TBE_MODULES'] = $temp_TBE_MODULES;
            $GLOBALS['TBE_MODULES']['_configuration']['nitsan'] = [
                'iconIdentifier' => 'module-nsbasetheme',
                'labels' => 'LLL:EXT:ext_key/Resources/Private/Language/BackendModule.xlf',
                'name' => 'nitsan'
            ];
        }

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
            'NITSAN.NsBasetheme',
            'nitsan', // Make module a submodule of 'nitsan'
            'nsbasethememodule', // Submodule key
            '', // Position
            [
                'NsBasethemeModule' => 'generalSettings, seoSettings, gdprSettings, styleSettings, integrationSettings, aboutExtension, saveConstant',
            ],
            [
                'access' => 'user,group',
                'icon' => 'EXT:ext_key/Resources/Public/Icons/Extension.svg',
                'labels' => 'LLL:EXT:ext_key/Resources/Private/Language/locallang_basethememodule.xlf',
                'navigationComponentId' => ($isVersion9Up ? 'TYPO3/CMS/Backend/PageTree/PageTreeElement' : 'typo3-pagetree'),
                'inheritNavigationComponentFromMainModule' => false
            ]
        );
    }
}

// Add default include static TypoScript (for root page)
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    $_EXTKEY,
    'Configuration/TypoScript',
    '[NITSAN] Parent theme'
);
if (version_compare(TYPO3_branch, '9.0', '>')) {
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['Backend\Template\Components\ButtonBar']['getButtonsHook'][] = 'NITSAN\NsBasetheme\Hooks\SaveCloseHook->addSaveCloseButton';
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['Backend\Template\Components\ButtonBar']['getButtonsHook'][] = 'NITSAN\NsBasetheme\Hooks\SaveCloseHook->addSaveShowButton';
}
